import React from "react";
import {
  Box,
  Heading,
  Link,
  Image,
  Text,
  HStack,
  Tag,
  Container,
  List,
  ListItem
} from "@chakra-ui/react";

const Details = ({ data }) => {
  return (
    <>
      {!data ? (
        ""
      ) : (
        <>
          <Container maxW={"7xl"} p="12">
            <Heading as="h1">Details {data.name}</Heading>
            <Box
              marginTop={{ base: "1", sm: "5" }}
              display="flex"
              flexDirection={{ base: "column", sm: "row" }}
              justifyContent="space-between"
            >
              <Box
                display="flex"
                flex="1"
                marginRight="3"
                position="relative"
                alignItems="center"
              >
                <Box
                  width={{ base: "100%", sm: "85%" }}
                  zIndex="2"
                  marginLeft={{ base: "0", sm: "5%" }}
                  marginTop="5%"
                >
                  <Link
                    textDecoration="none"
                    _hover={{ textDecoration: "none" }}
                  >
                    <Image
                      borderRadius="lg"
                      src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${data.id}.svg`}
                      alt="some good alt text"
                      objectFit="contain"
                    />
                  </Link>
                </Box>
                <Box zIndex="1" width="100%" position="absolute" height="100%">
                  <Box backgroundSize="20px 20px" opacity="0.4" height="100%" />
                </Box>
              </Box>
              <Box
                display="flex"
                flex="1"
                flexDirection="column"
                justifyContent="center"
                marginTop={{ base: "3", sm: "0" }}
              >
                <HStack spacing={2} mb={4}>
                  {data.abilities.map((poke, index) => {
                    return (
                      <Tag
                        size={"md"}
                        variant="solid"
                        colorScheme="orange"
                        key={index + 1}
                      >
                        {poke.ability.name}
                      </Tag>
                    );
                  })}
                </HStack>
                <Box>
                  <List spacing={2}>
                    {data.stats.map((poke) => {
                      return (
                        <ListItem>
                          <Text as={"span"} fontWeight={"bold"} mr={2}>
                            {poke.stat.name} :
                          </Text>
                          {poke.base_stat}
                        </ListItem>
                      );
                    })}
                  </List>
                </Box>
              </Box>
            </Box>
          </Container>
        </>
      )}
    </>
  );
};

export default Details;
