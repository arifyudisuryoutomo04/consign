import React, { useState } from "react";
import {
  Container,
  SimpleGrid,
  Flex,
  Text,
  Image,
  Stack,
  Heading,
  Input,
  Button,
  useColorModeValue
} from "@chakra-ui/react";
import { connect } from "react-redux";
import { getIndPokemon } from "./redux/actions";
import { Link } from "react-router-dom";
import Hero from "./components/hero";
import { Search2Icon } from "@chakra-ui/icons";

const HomePage = ({ pokemon, loading, props, infoPokemon }) => {
  const [searchTerm, setSearchTerm] = useState("");
  return (
    <>
      <Hero />
      <Container maxW={"7xl"} p="12">
        <Flex align={"center"} justify={"center"}>
          <Stack
            style={{
              marginTop: "-7%",
              marginBottom: "5%"
            }}
            boxShadow={"2xl"}
            bg={useColorModeValue("white", "gray.700")}
            rounded={"xl"}
            p={10}
            spacing={8}
            align={"center"}
          >
            <Stack align={"center"} spacing={2}>
              <Heading fontSize={"3xl"}>Search</Heading>
            </Stack>
            <Stack
              spacing={4}
              direction={{ base: "column", md: "row" }}
              w={"full"}
            >
              <Input
                type={"text"}
                placeholder={"Name Pokemon"}
                color={useColorModeValue("gray.800", "gray.200")}
                bg={useColorModeValue("gray.100", "gray.600")}
                rounded={"full"}
                border={0}
                _focus={{
                  bg: useColorModeValue("gray.200", "gray.800"),
                  outline: "none"
                }}
                onChange={(event) => {
                  setSearchTerm(event.target.value);
                }}
              />
              <Button
                bg={"#E53E3E"}
                rounded={"full"}
                color={"white"}
                flex={"1 0 auto"}
                _hover={{ bg: "blue.500" }}
                _focus={{ bg: "blue.500" }}
                onChange={(event) => {
                  setSearchTerm(event.target.value);
                }}
              >
                <Search2Icon />
              </Button>
            </Stack>
          </Stack>
        </Flex>

        <SimpleGrid columns={{ base: 1, md: 3 }} spacing={10}>
          {loading ? (
            <h1>Loading...</h1>
          ) : (
            pokemon
              .filter((val) => {
                if (searchTerm == "") {
                  return val;
                } else if (
                  val.name.toLowerCase().includes(searchTerm.toLowerCase())
                ) {
                  return val;
                }
              })
              .map((item, index) => {
                return (
                  <Link
                    key={index + 1}
                    to={`/${item.name}`}
                    onClick={() => infoPokemon(item)}
                  >
                    <Flex
                      h="282px"
                      justifyContent="center"
                      alignItems="center"
                      borderRadius="10px"
                      shadow={"lg"}
                    >
                      <Image
                        borderRadius="full"
                        boxSize="200px"
                        src={item.sprites.front_default}
                        alt={item.name}
                      />
                      <Text>{item.name}</Text>
                    </Flex>
                  </Link>
                );
              })
          )}
        </SimpleGrid>
      </Container>
    </>
  );
};

const mstp = (state) => {
  return {
    isLoading: state.pkr.isLoading,
    isError: state.pkr.isError,
    errorMsg: state.pkr.error,
    pokeNames: state.pkr.pokeNames
  };
};

export default connect(mstp, { getIndPokemon })(HomePage);
