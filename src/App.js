import React, { useEffect, useState } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import HomePage from "./HomePage";
import Details from "./Details";
import { connect } from "react-redux";
import { getPokemon } from "./redux/actions";
import NotFound from "./404";
import axios from "axios";

function App(props) {
  useEffect(() => {
    props.getPokemon(
      `https://pokeapi.co/api/v2/pokemon?limit=10&offset=${currentOffset}`
    );
  }, []);
  let currentOffset = 0;
  const [pokeData, setPokeData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [pokeDex, setPokeDex] = useState();

  const [url, setUrl] = useState(
    `https://pokeapi.co/api/v2/pokemon?limit=10&offset=${currentOffset}`
  );

  const pokeFun = async () => {
    const res = await axios.get(url);
    getPoke(res.data.results);
    setLoading(false);
  };

  const getPoke = async (res) => {
    res.map(async (item) => {
      const result = await axios.get(item.url);
      setPokeData((state) => {
        state = [...state, result.data];
        return state;
      });
    });
  };

  const handleScroll = (e) => {
    console.log(e.target.documentElement.scrollTop);
    console.log(window.innerHeight);
    console.log(e.target.documentElement.scrollHeight);

    const scrollHeight = e.target.documentElement.scrollHeight;
    const currentHeight = Math.ceil(
      e.target.documentElement.scrollTop + window.innerHeight
    );
    if (currentHeight + 1 >= scrollHeight) {
      pokeFun();
    }
  };

  useEffect(() => {
    pokeFun();
    // Infinite Scroller with React Hooks
    window.addEventListener("scroll", handleScroll);
  }, []);

  return (
    <BrowserRouter>
      <Routes>
        <Route
          path="/"
          element={
            <HomePage
              pokemon={pokeData}
              loading={loading}
              props
              infoPokemon={(poke) => setPokeDex(poke)}
            />
          }
        />
        <Route path="/:name" element={<Details data={pokeDex} />} />

        <Route path="*" element={<NotFound />} />
      </Routes>
    </BrowserRouter>
  );
}

const mstp = (state) => {
  return {
    isLoading: state.pkr.isLoading,
    isError: state.pkr.isError,
    errorMsg: state.pkr.error,
    pokemon: state.pkr.pokemon
  };
};

export default connect(mstp, { getPokemon })(App);
