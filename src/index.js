import React, { StrictMode } from "react";
import * as ReactDOM from "react-dom/client";
import App from "./App";
import { ChakraProvider, theme } from "@chakra-ui/react";
import Navbar from "./components/navbar";
import Footer from "./components/footer";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import logger from "redux-logger";
import rootReducer from "./redux/reducers";
// import { store } from "./redux/store"; --> deaults redux-toolkit

const store = createStore(rootReducer, applyMiddleware(thunk, logger));
const container = document.getElementById("root");
const root = ReactDOM.createRoot(container);

root.render(
  <Provider store={store}>
    <StrictMode>
      <ChakraProvider theme={theme}>
        <Navbar />
        <App />
        <Footer />
      </ChakraProvider>
    </StrictMode>
  </Provider>
);
